terraform {
  backend "s3" {
    region               = "eu-west-1"
    bucket               = "nodejs-fargate-ecr-terraform-backend"
    encrypt              = true
    key                  = "tfstate"
    workspace_key_prefix = "default"
  }
}
provider "aws" {
  region = "eu-west-1"
}
